import React from "react"
import s from "./CategoryItem.module.css"
import {Link} from "react-router-dom"

interface ICategoryItem {
    readonly link: string
    readonly title: string
    readonly img: string
}

const CategoryItem = (props: ICategoryItem) => {
    return (
        <div className={s.categoryContainer}>
            <Link to={"/category?typeId=" + props.link} style={{backgroundImage: `url(${props.img})`}}>
                <div className={s.categoryBlock}><span>{props.title}</span></div>
            </Link>
        </div>
    )
}

export default CategoryItem