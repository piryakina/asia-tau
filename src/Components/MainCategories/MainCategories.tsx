import React from "react"
import CategoryItem from "../CategoryItem/CategoryItem"
import imgLight from "../../Assets/Categories/lightning.png"
import imgAudio from "../../Assets/Categories/audio.png"
import s from "./MainCategories.module.css"

const MainCategories = () => {
    return (
        <div className={s.mainContainer}>
            <CategoryItem link="4" title="LIGHTNING EQUIPMENT" img={imgLight}/>
            <CategoryItem link="3" title="AUDIO EQUIPMENT" img={imgAudio}/>
        </div>
    )
}

export default MainCategories