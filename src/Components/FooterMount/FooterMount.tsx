import React from "react"
import mount from "./../../Assets/Foot/mount.svg"
import s from "../FooterMount/FooterMount.module.css"

const FooterMount = () => {
    return (
        <div className={s.footerContainer}>
            <div className={s.footerWrapper}>
                <span>@Copyright LC “Asiatau”. All rights reserved. 2023</span>
            </div>
            <img src={mount} alt=""/>
        </div>
    )
}

export default FooterMount