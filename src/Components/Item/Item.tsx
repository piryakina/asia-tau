import React from "react"
import s from "./Item.module.css"
import {Link} from "react-router-dom"
import {backDomain} from "../../Domain"
import img from "../../Assets/TEMP/img.svg"

interface IItem {
    readonly seriesId?: number
    readonly productId?: string
    readonly categoryId: number
    readonly name: string
    readonly brandId: number
    readonly typeId: number
    readonly brand?: string
    readonly path?: string
    readonly page: string
}

const Item = (props: IItem) => {
    return (
        <div className={s.brandCategoryContainer}>
            {props.page === "series" ?
                <Link
                    to={`../series?typeId=${props.typeId}&brand=${props.brand}&brandId=${props.brandId}&categoryId=${props.categoryId}`}>
                    <div className={s.imgContainer}>
                        <img src={`${backDomain}img/${props.path}`} alt=""/>
                    </div>
                    <p className={s.type}>{props.name}</p>
                </Link>
                : null}
            {props.page === "models" ?
                <Link
                    to={`../models?typeId=${props.typeId}&brand=${props.brand}&brandId=${props.brandId}&categoryId=${props.categoryId}&seriesId=${props.seriesId}`}>
                    <div className={s.imgContainer}>
                        <img src={`${backDomain}img/${props.path}`} alt=""/>
                    </div>
                    <p className={s.type}>{props.name}</p>
                </Link>
                : null}
            {props.page === "product" ?
                <Link
                    to={`../product?typeId=${props.typeId}&brandId=${props.brandId}&categoryId=${props.categoryId}&seriesId=${props.seriesId}&productId=${props.productId}`}>
                    <div className={s.imgContainer}>
                        <img src={`${backDomain}img/${props.path}`} alt=""/>
                    </div>
                    <p className={s.type}>{props.name}</p>
                </Link>
                : null}
        </div>
    )
}

export default Item