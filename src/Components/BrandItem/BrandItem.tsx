import React from "react"
import s from "./BrandItem.module.css"
import {Link} from "react-router-dom"
import {backDomain} from "../../Domain"

interface IBrand {
    readonly brand: string
    readonly brandId: number
    readonly typeId: number
}

const BrandItem = (props: IBrand) => {
    return (
        <div className={s.brandContainer}>
            <Link to={`../brand?typeId=${props.typeId}&brandId=${props.brandId}&brand=${props.brand}`}>
                <img src={`${backDomain}brands/${props.brand}.png`} alt=""/>
            </Link>
        </div>
    )
}

export default BrandItem