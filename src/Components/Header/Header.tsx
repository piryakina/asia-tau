import React, {useEffect, useState} from "react"
import logo from "./../../Assets/Head/logo.svg"
import tau from "./../../Assets/Head/asia-tau.svg"
import s from "./Header.module.css"
import {Link} from "react-router-dom"

const Header = () => {
    const [width, setWidth] = useState<number>(window.innerWidth)
    const breakpoint = 1226
    useEffect(() => {
        const handleResizeWindow = () => setWidth(window.innerWidth)
        window.addEventListener("resize", handleResizeWindow)
        return () => {
            window.removeEventListener("resize", handleResizeWindow)
        }
    }, [])
    return (
        <div className={s.headerContainer}>
            {breakpoint > width ? <>
                    <div className={s.headerItemAdaptive}>
                        <Link to="/">
                            <div className={s.logoContainer}><img src={logo} alt=""/></div>
                        </Link>
                    </div>
                    <div className={s.menuItem}>
                        <img src={tau} alt=""/>
                    </div>
                </> :
                <>
                    <div className={s.headerItem}>
                        <Link to="/" className={s.logoContainer}> <img src={logo} alt=""/></Link>
                        <Link to="/category?typeId=4">LIGHTNING EQUIPMENT</Link>
                        <Link to="/category?typeId=3">AUDIO EQUIPMENT</Link>
                    </div>
                    <div className={s.headerItem}>
                        <Link to="/contact">CONTACT</Link>
                        <Link to="/company">COMPANY</Link>
                        <a href="/"><img src={tau} alt=""/></a>
                    </div>
                </>
            }
        </div>
    )
}

export default Header