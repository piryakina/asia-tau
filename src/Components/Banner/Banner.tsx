import React from "react"
import s from "./Banner.module.css"
import {Swiper, SwiperSlide} from "swiper/react"
import 'swiper/css'
import {Autoplay} from "swiper"

interface IBanner {
    readonly img: string[]
}

const Banner = (props: IBanner) => {
    return (
        <>
            <Swiper
                autoplay={{
                    delay: 2200
                }}
                speed={1500}
                loop={true}
                modules={[Autoplay]}
                className={s.swiper}>
                {props.img.map((i) => {
                    return (
                        <SwiperSlide><img className={s.banner} src={i} alt=""/></SwiperSlide>
                    )
                })
                }
            </Swiper></>
    )
}

export default Banner