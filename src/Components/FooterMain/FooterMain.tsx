import React, {useEffect, useState} from "react"
import s from "./FooterMain.module.css"
import {Link} from "react-router-dom"

const FooterMain = () => {
    const [width, setWidth] = useState<number>(window.innerWidth)
    const breakpoint = 1200
    useEffect(() => {
        const handleResizeWindow = () => setWidth(window.innerWidth)
        window.addEventListener("resize", handleResizeWindow)
        return () => {
            window.removeEventListener("resize", handleResizeWindow)
        }
    }, [])
    return (
        <div className={s.footerContainer}>
            {width > breakpoint ?
                <div className={s.footerWrapper}>
                    <span>@Copyright LC “Asiatau”. All rights reserved. 2023</span>
                </div> :
                <div className={s.footerAdaptive}>
                    <Link to="/category?typeId=4">LIGHTNING EQUIPMENT</Link>
                    <Link to="/category?typeId=3">AUDIO EQUIPMENT</Link>
                    <Link to="/contact">CONTACT</Link>
                    <Link to="/company">COMPANY</Link>
                    <div className={s.footerWrapperAdaptive}>
                        <span>@Copyright LC “Asiatau”. All rights reserved. 2023</span>
                    </div>
                </div>
            }
        </div>
    )
}

export default FooterMain