import React from 'react'
import preload from "./../../Assets/TEMP/preload.gif"
import s from './Preload.module.css'

const Preload = () => {
    return (
        <div className={s.preloadContainer}>
            <img src={preload} alt=""/>
        </div>
    )
}

export default Preload