import React from "react"
import FooterMain from "../../Components/FooterMain/FooterMain"
import s from "./Contact.module.css"

const Contact = () => {
    return (
        <div className={s.container}>
            <div className={s.containerWrapper}>
                <div className={s.mapsContainer}>
                    <iframe className={s.map}
                            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d731.19996021571!2d74.53357002926509!3d42.855959398697344!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x389ec8fb4d29319d%3A0x244aec112c90b02d!2z0JvQtdC90LjQvdC-0L_QvtC70YzRgdC60LjQuSDQv9C10YAuLCDQkdC40YjQutC10LosINCa0YvRgNCz0YvQt9GB0YLQsNC9!5e0!3m2!1sru!2sru!4v1680160377240!5m2!1sru!2sru"
                            referrerPolicy="no-referrer-when-downgrade"></iframe>
                </div>
                <div className={s.textContainer}>
                    <h2 className={s.title}>CONTACT</h2>
                    <p className={s.text}>Leninopolskaya st. 4
                        Bishkek, Kyrgyzstan
                        +996(550)301124
                        info@asiatau.kg</p>
                </div>
            </div>
            <FooterMain/>
        </div>
    )
}

export default Contact