import React, {useEffect, useState} from "react"
import {Link, useSearchParams} from "react-router-dom"
import {backDomain} from "../../Domain"
import Preload from "../../Components/Preload/Preload"
import s from "../Brand/Brand.module.css"
import img from "../../Assets/TEMP/img.svg"
import Martin from "../../Assets/LightBrands/img.png"
import Item from "../../Components/Item/Item"
import FooterMain from "../../Components/FooterMain/FooterMain"

const Model = () => {
    const [query] = useSearchParams()
    const [data, setData] = useState<any>()
    const typeId = Number(query.get("typeId"))
    const brandId = Number(query.get("brandId"))
    const categoryId = Number(query.get("categoryId"))
    const seriesId = Number(query.get("seriesId"))
    const brand = query.get("brand")
    const [preload, setPreload] = useState(false)
    useEffect(() => {
        if (typeId !== null && brandId !== null && categoryId !== null) {
            fetch(`${backDomain}products?typeId=${typeId}&brandId=${brandId}&categoryId=${categoryId}&seriesId=${seriesId}`, {
                method: "GET",
                headers: {
                    "Access-Control-Allow-Origin": "*",
                    "ngrok-skip-browser-warning": "69420",
                },
            })
                .then((res) => res.json())
                .then((data) => {
                    setData(data)
                    setPreload(true)
                });
        }
    }, [])
    return (
        <>
            {!preload ? <Preload/> :
                <>
                    <div className={s.imgContainer}>
                        <Link to={"Martin"}>
                            <img className={s.image} src={`${backDomain}brands/${brand}.png`} alt={""}/>
                        </Link>
                    </div>
                    <div>
                        {typeId === 3 ? <>
                                <div><p className={s.title}>AUDIO EQUIPMENT</p></div>
                            </>
                            : null}
                        {typeId === 4 ? <>
                                <div><p className={s.title}>LIGHTNING EQUIPMENT</p></div>
                            </>
                            : null}
                        <h2 className={s.browse}>Browse by models</h2>
                    </div>
                    <div className={s.category}>
                        {data ? data.products.map((i: { product_id: string, title: string, path: string }) => {
                                return (
                                    <><Item page={"product"} brandId={brandId} typeId={typeId} categoryId={categoryId}
                                            name={i.title} seriesId={seriesId} path={i.path} productId={i.product_id}/></>
                                )
                            }
                        ) : null
                        }
                    </div>
                </>
            }
            <FooterMain/>
        </>
    )
}

export default Model