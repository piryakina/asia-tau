import React, {useEffect, useState} from "react"
import img from "../../Assets/TEMP/img.svg"
import s from "../Brand/Brand.module.css"
import {Link, useSearchParams} from "react-router-dom"
import Item from "../../Components/Item/Item"
import FooterMain from "../../Components/FooterMain/FooterMain"
import {backDomain} from "../../Domain"
import Preload from "../../Components/Preload/Preload"

const Series = () => {
    const [query] = useSearchParams()
    const [data, setData] = useState<any>()
    const typeId = Number(query.get("typeId"))
    const brandId = Number(query.get("brandId"))
    const categoryId = Number(query.get("categoryId"))
    const brand = query.get("brand")
    const [preload, setPreload] = useState(false)
    useEffect(() => {
        if (typeId !== null && brandId !== null && categoryId !== null) {
            fetch(`${backDomain}series?typeId=${typeId}&brandId=${brandId}&categoryId=${categoryId}`, {
                method: "GET",
                headers: {
                    "Access-Control-Allow-Origin": "*",
                    "ngrok-skip-browser-warning": "69420",
                },
            })
                .then((res) => res.json())
                .then((data) => {
                    setData(data)
                    setPreload(true)
                })
        }
    }, [])
    return (
        <>
            {!preload ? <Preload/> :
                <>
                    <div className={s.imgContainer}>
                        <Link to={`brand?typeId=${typeId}&brandId=${brandId}&brand=${brand}`}>
                            <img className={s.image} src={`${backDomain}brands/${brand}.png`} alt={""}/>
                        </Link>
                    </div>
                    <div>
                        {typeId === 3 ? <>
                                <div><p className={s.title}>AUDIO EQUIPMENT</p></div>
                            </>
                            : null}
                        {typeId === 4 ? <>
                                <div><p className={s.title}>LIGHTNING EQUIPMENT</p></div>
                            </>
                            : null}
                        <h2 className={s.browse}>Browse by series</h2>
                    </div>
                    <div className={s.category}>
                        {data ? data.products.map((i: { series_id: number, series_value: string, path: string, brand_name: string }) => {
                                return (
                                    <><Item page={"models"} brandId={brandId} typeId={typeId} categoryId={categoryId}
                                            name={i.series_value} seriesId={i.series_id} path={i.path}
                                            brand={i.brand_name}/></>
                                )
                            }
                        ) : null
                        }
                    </div>
                </>
            }
            <FooterMain/>
        </>
    )
}

export default Series