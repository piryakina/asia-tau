import React, {useEffect, useState} from "react"
import Preload from "../../Components/Preload/Preload"
import {useSearchParams} from "react-router-dom"
import s from "./Product.module.css"
import {backDomain} from "../../Domain"
import {Swiper, SwiperSlide} from "swiper/react"
import 'swiper/css'
import 'swiper/css/bundle'
import {Navigation} from "swiper"
import prev from "../../Assets/Swiper/prevBlack.svg"
import next from "../../Assets/Swiper/nextBlack.svg"
import FooterMain from "../../Components/FooterMain/FooterMain"

const Product = () => {
    const [query] = useSearchParams()
    const [data, setData] = useState<any>()
    const productId = query.get("productId")
    const [preload, setPreload] = useState(false)
    const navigationPrevRef = React.useRef(null)
    const navigationNextRef = React.useRef(null)
    useEffect(() => {
        if (productId !== null) {
            fetch(`${backDomain}product/${productId}`, {
                method: "GET",
                headers: {
                    "Access-Control-Allow-Origin": "*",
                    "ngrok-skip-browser-warning": "69420",
                },
            })
                .then((res) => res.json())
                .then((data) => {
                    setData(data)
                    setPreload(true)
                })
        }
    }, [])
    return (
        <>{!preload ? <Preload/> :
            <>
                <div className={s.titleContainer}>
                    <h2 className={s.title}>{data.product[0].title}</h2>
                    <p className={s.description}> {data.product[0].description}</p>
                </div>
                <div className={s.container}>
                    <div className={s.textContainer}>
                        {data.product[0].specification}
                    </div>
                    <div className={s.swiperContainer}>
                        <Swiper
                            cssMode={true}
                            navigation={{
                                prevEl: navigationPrevRef.current,
                                nextEl: navigationNextRef.current,
                            }}
                            modules={[Navigation]}
                            className={s.mySwiper}
                            loop={true}
                        >
                            {data.product[0].images.map((i: any) => {
                                return (
                                    <SwiperSlide>
                                        <div className={s.swiperSlider}><img src={`${backDomain}img/${i.path}`} alt={""}
                                                                             key={i + 3}/></div>
                                    </SwiperSlide>
                                )
                            })}
                            <img ref={navigationPrevRef} className="swiper-button-prev" src={prev}
                                 alt={""}/>
                            <img ref={navigationNextRef} className="swiper-button-next" src={next}
                                 alt={""}/>
                        </Swiper>
                    </div>
                </div>
            </>}
            <FooterMain/>
        </>
    )
}

export default Product