import React, {useEffect, useState} from "react"
import Banner from "../../Components/Banner/Banner"
import lightBanner from "../../Assets/Light/banner.svg"
import audioBanner from "../../Assets/Audio/banner.svg"
import img2 from "../../Assets/Audio/img2.svg"
import img3 from "../../Assets/Audio/img3.svg"
import img4 from "../../Assets/Audio/img4.svg"
import FooterMain from "../../Components/FooterMain/FooterMain"
import s from "./Category.module.css"
import {useSearchParams} from "react-router-dom"
import {backDomain} from "../../Domain"
import BrandItem from "../../Components/BrandItem/BrandItem"
import Preload from "../../Components/Preload/Preload"

const Category = () => {
    const [query] = useSearchParams()
    const typeId = Number(query.get("typeId"))
    const [data, setData] = useState<any>()
    const [preload, setPreload] = useState(false)
    const audio: string[] = [audioBanner, img2, img3, img4]
    const light: string[] = [lightBanner]
    useEffect(() => {
        if (typeId !== null) {
            fetch(`${backDomain}brands/?typeId=${typeId}`, {
                method: "GET",
                headers: {
                    "Access-Control-Allow-Origin": "*",
                    "ngrok-skip-browser-warning": "69420",
                },
            })
                .then((res) => res.json())
                .then((data) => {
                    setData(data)
                    setPreload(true)
                })
        }
    }, [typeId])
    useEffect(() => {
        setPreload(false)
    }, [typeId])
    return (
        <>
            {!preload ? <Preload/> :
                <>
                    {typeId === (3) ?
                        <>
                            <Banner img={audio}/>
                            <p className={s.title}>AUDIO EQUIPMENT</p>
                        </> : null}
                    {typeId === (4) ?
                        <>
                            <Banner img={light}/>
                            <p className={s.title}>LIGHTNING EQUIPMENT</p>
                        </> : null
                    }
                    <h2 className={s.browse}>Browse by brands</h2>
                    <div className={s.brandsContainer}>
                        {data ? data.brands.map((i: { brand_id: number, type_id: number, brand_name: string }, index: number) => {
                            return (
                                <BrandItem brand={i.brand_name} brandId={i.brand_id} typeId={i.type_id} key={index}/>
                            )
                        }) : null}
                    </div>
                    <FooterMain/>
                </>
            }
        </>
    )
}

export default Category