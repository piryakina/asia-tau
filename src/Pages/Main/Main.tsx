import React from "react"
import MainCategories from "../../Components/MainCategories/MainCategories"
import FooterMain from "../../Components/FooterMain/FooterMain"
import Banner from "../../Components/Banner/Banner"
import img from "../../Assets/Main/main.svg"
import img2 from "../../Assets/Main/main1.svg"
import img3 from "../../Assets/Main/main2.svg"
import img4 from "../../Assets/Main/main3.svg"
import img5 from "../../Assets/Main/main4.svg"
import img6 from "../../Assets/Main/main5.svg"
import img7 from "../../Assets/Main/main6.svg"

const Main = () => {
    const images: string[] = [img, img2, img3, img4, img5, img6, img7]
    return (
        <>
            <Banner img={images}/>
            <MainCategories/>
            <FooterMain/>
        </>
    )
}

export default Main