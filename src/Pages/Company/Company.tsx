import React from "react"
import FooterMount from "../../Components/FooterMount/FooterMount"
import Logo from "./../../Assets/Company/logo.svg"
import s from "./Company.module.css"
import {Swiper, SwiperSlide} from "swiper/react"
import 'swiper/css'
import 'swiper/css/bundle'
import img1 from "./../../Assets/Company/1.jpg"
import img2 from "./../../Assets/Company/2.jpg"
import img3 from "./../../Assets/Company/3.jpg"
import img4 from "./../../Assets/Company/4.jpg"
import img5 from "./../../Assets/Company/5.jpg"
import img6 from "./../../Assets/Company/6.jpg"
import {Navigation} from "swiper"
import next from "./../../Assets/Swiper/next.svg"
import prev from "./../../Assets/Swiper/prev.svg"

const Company = () => {
    const navigationPrevRef = React.useRef(null)
    const navigationNextRef = React.useRef(null)
    return (
        <>
            <div className={s.logoContainer}>
                <img src={Logo} alt={""}/>
            </div>
            <div className={s.textContainer}>
                <p>The "Asiatau" company has beed operating in the Kyrgyzstan market over 5 years. We are specializes in
                    business and enterprise AV systems. We provide the finest installations of audio video systems,
                    dedicated projectors, video walls, screens and other equipment for corporations, stadiums, theaters,
                    restaurants and more.
                    View our projects
                    Mausoleum of the Karakhanid era in Uzgen
                    The Equestian Hippodrome
                    Main Historical Museum
                    Opera and Ballet theatre
                    Presidential palace
                    Main sport arena 'SPARTAK' </p>
            </div>
            <Swiper
                cssMode={true}
                navigation={{
                    prevEl: navigationPrevRef.current,
                    nextEl: navigationNextRef.current,
                }}
                modules={[Navigation]}
                className={s.mySwiper}
                loop={true}
            >
                <SwiperSlide>
                    <div className={s.swiperSlider}><img src={img1} alt={""}/></div>
                </SwiperSlide>
                <SwiperSlide>
                    <div className={s.swiperSlider}><img src={img2} alt={""}/></div>
                </SwiperSlide>
                <SwiperSlide>
                    <div className={s.swiperSlider}><img src={img3} alt={""}/></div>
                </SwiperSlide>
                <SwiperSlide>
                    <div className={s.swiperSlider}><img src={img4} alt={""}/></div>
                </SwiperSlide>
                <SwiperSlide>
                    <div className={s.swiperSlider}><img src={img5} alt={""}/></div>
                </SwiperSlide>
                <SwiperSlide>
                    <div className={s.swiperSlider}><img src={img6} alt={""}/></div>
                </SwiperSlide>
                <img ref={navigationPrevRef} className="swiper-button-prev" src={prev}
                     alt={""}/>
                <img ref={navigationNextRef} className="swiper-button-next" src={next}
                     alt={""}/>
            </Swiper>
            <FooterMount/>
        </>
    )
}

export default Company