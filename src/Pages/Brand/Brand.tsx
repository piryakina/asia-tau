import React, {useEffect, useState} from "react"
import s from "./Brand.module.css"
import Item from "../../Components/Item/Item"
import img from "../../Assets/TEMP/img.svg"
import {Link, useSearchParams} from "react-router-dom"
import FooterMain from "../../Components/FooterMain/FooterMain"
import {backDomain} from "../../Domain"
import Preload from "../../Components/Preload/Preload"

const Brand = () => {
    const [query] = useSearchParams()
    const [data, setData] = useState<any>()
    const typeId = Number(query.get("typeId"))
    const brandId = Number(query.get("brandId"))
    const brand = query.get("brand")
    const [preload, setPreload] = useState(false)
    useEffect(() => {
        if (typeId !== null && brandId !== null) {
            fetch(`${backDomain}categories?typeId=${typeId}&brandId=${brandId}`, {
                method: "GET",
                headers: {
                    "Access-Control-Allow-Origin": "*",
                    "ngrok-skip-browser-warning": "69420",
                },
            })
                .then((res) => res.json())
                .then((data) => {
                    setData(data)
                    setPreload(true)
                })
        }
    }, [])
    return (
        <> {!preload ? <Preload/> :
            <>
                <div className={s.imgContainer}>
                    <Link to={`/brand?typeId=${typeId}&brandId=${brandId}&brand=${brand}`}>
                        <img className={s.image} src={`${backDomain}brands/${brand}.png`} alt={""}/>
                    </Link>
                </div>
                {typeId === 3 ? <>
                        <div><p className={s.title}>AUDIO EQUIPMENT</p></div>
                    </>
                    : null}
                {typeId === 4 ? <>
                        <div><p className={s.title}>LIGHTNING EQUIPMENT</p></div>
                    </>
                    : null}
                <h2 className={s.browse}>Browse by categories</h2>
                <div className={s.category}>
                    {data ? data.categories.map((i: { category_id: number, category_name: string, brand_id: number, type_id: number, brand_name: string, path: string }, index: number) => {
                        return (
                            <><Item brand={i.brand_name} categoryId={i.category_id} name={i.category_name}
                                    brandId={i.brand_id}
                                    typeId={i.type_id} page={"series"} key={index + brandId} path={i.path}/></>
                        )
                    }) : null}
                </div>
                <FooterMain/>
            </>
        }</>
    )
}

export default Brand