import React from 'react'
import Header from "./Components/Header/Header"
import {Route, Routes} from "react-router-dom"
import Main from "./Pages/Main/Main"
import Company from "./Pages/Company/Company"
import Contact from "./Pages/Contact/Contact"
import Category from "./Pages/Category/Category"
import Brand from "./Pages/Brand/Brand"
import Series from "./Pages/Series/Series"
import Model from "./Pages/Model/Model"
import Product from "./Pages/Product/Product"

function App() {
    return (
        <div className="App">
            <Header/>
            <Routes>
                <Route path="" element={<Main/>}/>
                <Route path="/contact" element={<Contact/>}/>
                <Route path="/company" element={<Company/>}/>
                <Route path="/category" element={<Category/>}/>
                <Route path="/brand" element={<Brand/>}/>
                <Route path="/series" element={<Series/>}/>
                <Route path="/models" element={<Model/>}/>
                <Route path="/product" element={<Product/>}/>
            </Routes>
        </div>
    )
}

export default App